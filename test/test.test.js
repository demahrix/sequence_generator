const assert = require('assert');
const { it } = require('mocha');
const SequenceGenerator = require('../sequence_generator');

it('should be unique', () => {

    const seq = new SequenceGenerator();
    const arr = new Array(1000000);
    let unique = true;

    for (let i=0; i<1000000; ++i) {
        arr[i] = seq.nextId();
    }

    for (let i=1; i < 1000000; ++i) {
        if (arr[i-1] == arr[i])
            unique = false;
    }

    assert(unique);
});
