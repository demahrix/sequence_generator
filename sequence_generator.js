/**
 * Les IDs generés sont composés de
 * * Epoch en millisecons: 41 bits. 
 * * Process ID: 10 bits
 * * Compteur local par machine: 12 bits.
 * Les 1-bit restant est le bit de signe et sa valeur est toujours à zero
 */
class SequenceGenerator {

    static UNUSED_BITS = 1; // Bit de signe, inutilisé toujours à 1
    static EPOCH_BITS = 41;
    static NODE_ID_BITS = 10;
    static SEQUENCE_BITS = 12;

    static MAX_NODE_ID = Math.pow(2, SequenceGenerator.NODE_ID_BITS) - 1;
    static MAX_SEQUENCE = Math.pow(2, SequenceGenerator.SEQUENCE_BITS) - 1;

    // Notre epoche 1er janvier 2018: 2018-01-30T12:16:58.822Z
    static CUSTOM_EPOCH = 1517314644018;

    /**
     * 
     * @param {number} nodeId 
     */
    constructor(nodeId = null) {
        if (nodeId == null) {
            this.nodeId = SequenceGenerator.createNodeId();
        } else if (nodeId < 0 || nodeId > SequenceGenerator.MAX_NODE_ID) {
            throw new Error();
        } else {
            this.nodeId = BigInt(nodeId);
        }

        this.lastTimestamp = -1;
        this.sequence = 0;
    }

    nextId() {
        let currentTimestamp = this.timestamp();

        // case anormal car le temps ne fait qu'avance
        if (currentTimestamp < this.lastTimestamp) {
            throw new Error("Invalid System Clock!");
        }

        if (currentTimestamp == this.lastTimestamp) {
            this.sequence = (this.sequence + 1) & SequenceGenerator.MAX_SEQUENCE;
            if (this.sequence == 0) {
                // Sequence terminé on attend la prochaine milliseonds
                currentTimestamp = this.waitNextMilliseconds(currentTimestamp);
            }
        } else {
            // Reinitialise la sequence à zero pour la prochaine milliseonds
            this.sequence = 0;
        }

        this.lastTimestamp = currentTimestamp;

        let id = BigInt(currentTimestamp) << BigInt(SequenceGenerator.NODE_ID_BITS + SequenceGenerator.SEQUENCE_BITS);
        id |= this.nodeId << BigInt(SequenceGenerator.SEQUENCE_BITS);
        id |= BigInt(this.sequence);
        return id;
    }

    /**
     * Donne le nombre de milliseconde passé depuis notre "custom epoch"
     * @returns {number}
     */
    timestamp() {
        return Date.now() - SequenceGenerator.CUSTOM_EPOCH;
    }

    /**
     * Bloque et attent la prochaine milliseconde
     */
    waitNextMilliseconds(currentTime) {
        while (currentTime == this.lastTimestamp) {
            currentTime = this.timestamp();
        }
        return currentTime;
    }

    /**
     * @returns {number}
     */
    static createNodeId() {
        return BigInt(process.pid & SequenceGenerator.MAX_NODE_ID);
    }

}

module.exports = SequenceGenerator;